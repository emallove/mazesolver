package com.aware.maze.solver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* 
 *  Aware Coding Pre-Interview Test
 *  
 *  Maze
 *  
 *  Goal: Write a program in either C++, C#, or Java which
 *  satisfies the requirements below and email back the source
 *  code when you are done.
 *  Priorities (in order):
 *  
 *      1) Implementation time, the source must be emailed back
 *          within 3 hours, sooner if possible. If you hit 3:30
 *          please just email what you have done. Returning code
 *          after the 3:30 hour mark will have a strong negative
 *          impact on your evaluation.
 *      2) Correctness of the solution
 *      3) Elegance of code, object oriented architecture
 *      4) Anything else you think makes a good program
 *  
 *  Requirements:
 *  
 *  -The program must use only standard libraries, and should
 *      compile and run on the default version of the complier.
 *  -The program should be executed from the command line and
 *      take one input parameter, the name of the input file. For
 *      example "my_maze_program.exe input_file1.txt"
 *  -The following layout represents a board. If you do not
 *      see the a nice layout, you might copy the board to a mono
 *      spaced editor such as �notepad.exe�
 *  
 *      o....
 *      xx.x.
 *      ...xx
 *      ..x..
 *      ...f.
 *      .....
 *      
 *  -The �o� represents your game piece on the board, there can
 *      only be one game piece on the board at any given point in
 *      time.
 *  -The �.� represents an empty space on the board where your
 *      game piece can move to.
 *  -The �x� represents an occupied space on the game board
 *      where your game piece cannot move to. NOTE: The positions
 *      of the occupied spaces will never change during the game.
 *  -The �f� represents the finish space on the game board.
 *      The goal is to move your game piece to this space in the
 *      fewest number of moves possible.
 *  -Your game piece can only move to adjacent unoccupied game
 *      spaces, either up, down, left, or right.
 *      Diagonal/orthogonal moves are not allowed.
 *  -The width and height of the input board should be dynamic
 *      based on the input file. In this example the board width
 *      is 5 and the height is 6, however based on the input file
 *      below the width would be 3 and height of 4. The width and
 *      height of the board will not change during the game.
 *  
 *          xxo
 *          x..
 *          ..x
 *          f..
 *  
 *  -To solve the game above the moves might look like the
 *  following boards.
 *  
 *          xxo
 *          x..
 *          ..x
 *          f..
 *          xx.
 *          x.o
 *          ..x
 *          f..
 *          xx.
 *          xo.
 *          ..x
 *          f..
 *          xx.
 *          x..
 *          .ox
 *          f..
 *          xx.
 *          x..
 *          ..x
 *          fo.
 *          xx.
 *          x..
 *          ..x
 *          o..
 *  
 *  -The output of the program should ONLY contain a listing of
 *  the row, column values for your game piece to reach the
 *  finish space in the fewest number of moves. The upper left
 *  space should be considered 0, 0. For example the game
 *  above would yield an output of:
 *  
 *      0,2
 *      1,2
 *      1,1
 *      2,1
 *      3,1
 *      3,0
 *  
 *  -If there are multiple correct answers to the algorithm,
 *  only one needs to be output to the screen.
 *  -If there are no possible correct answers then, the ONLY
 *  thing output to the screen should be �unsolvable�.
 *  NOTE: You may ask questions, please email and we will
 *  respond as quickly as possible.
 */

public class MazeSolver {

	private final static String STARTING_POINT_CHARACTER = "o";
	private final static String BLOCKADE_CHARACTER = "x";
	private final static String FREE_SPACE_CHARACTER = ".";
	private final static String FINISHING_POINT_CHARACTER = "f";

	public static void main(String[] args) {

		ArrayList<List<String>> maze = new ArrayList<List<String>>();
		ArrayList<List<Integer>> steps = new ArrayList<List<Integer>>();

		maze = readTextFile(args[0]);
		System.out.println(maze);

		steps = naiveSolve(maze);
		
		if (null != steps)
			System.out.println(steps);
		else
			System.out.println("unsolvable");

	}
	
	/*
	 * Naive solve method.
	 */
	private static ArrayList<List<Integer>> naiveSolve(ArrayList<List<String>> maze) {
		
		ArrayList<List<Integer>> steps = null;
		boolean solvable = false;
		
		/* Get the starting point, referred to in the maze as 'o' */
		ArrayList<Integer> start;
		
		// Start search leftwards (arbitrary choice)
		boolean movingLeft = true;
		
		// Have we searched every position in this row?
		boolean rowExhausted = false;

		try {
			start = findStart(maze);
			
			System.out.println("start = " + start);

			/*
			 * We assume a well-formed maze has been input. That is, all rows
			 * are of equal length.
			 */
			int i = start.get(0);
			int j = start.get(1);
			
			/* 
			 * Keep track of row steps, in case we need to backtrack
			 */
			int rowSteps = 0;
			
			/* 
			 * Algorithm
			 * 
			 * Search left (or right) until we hit a blockade or the margin.
			 * 		If there are more positions to explore in this row, backtrack
			 * 		and change direction - otherwise move on to the next row and repeat
			 * 
			 * If we find the finish spot, return.
			 * 
			 * Note: in this naive algorithm, we only ascend row-wise.
			 * 
			 * 
			 * THIS METHOD IS UNTESTED AND 25% IMPLEMENTED ... IF THAT :-\
			 * 
			 */
			
			while (i < maze.size()) {
				
				while (j < maze.get(0).size() && j > 0) {
					
					ArrayList<Integer> step = new ArrayList<Integer>(2);
					step.add(i);
					step.add(j);
					rowSteps++;
					
					if (FINISHING_POINT_CHARACTER.equals(maze.get(i).get(j))) {
						solvable = true;
						return steps;
						
					} else if (movingLeft) {
						if (j != 0) {
							if (maze.get(i).get(j - 1).equals(FREE_SPACE_CHARACTER))
								j--;
							else if (maze.get(i).get(j - 1).equals(BLOCKADE_CHARACTER)) {
								backtrack(steps, rowSteps);
								movingLeft = !movingLeft;
							}

						} else {
							movingLeft = !movingLeft;
							if (j + 1 < maze.size())
								if (maze.get(i + 1).get(j).equals(FREE_SPACE_CHARACTER))
									i++;
						}
						
					} else if (!movingLeft) {
						if (j != maze.get(i).size()) {
							if (maze.get(i).get(j + 1).equals(FREE_SPACE_CHARACTER))
								j++;
						}
						
					} else if (j == 0) {
						if (movingLeft) {
							i++;
							movingLeft = !movingLeft;
						}
						
					} else if (maze.get(i).get(j + 1).equals(BLOCKADE_CHARACTER)) {
						if (rowExhausted)
							i++;
					}

				}
			}

		} catch (MazeDoesNotContainAStartingPoint e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (solvable)
			return steps;
		else
			return null;
		
	}
	
	/*
	 * Exercise left to reader :-)
	 * 
	 * TODO: http://en.wikipedia.org/wiki/A*_search_algorithm
	 * 
	 */
	private static ArrayList<List<Integer>> aStarSolve(ArrayList<List<String>> maze) throws Exception {
		
		/* 
		 * Silence javac for now, while this is unimplemented
		 */
		if (true)
			throw new Exception("A * Search is not implemented!");
		
		return null;
	}

	/* 
	 * Read a maze from a file
	 * 
	 * Source: http://www.vogella.com/tutorials/JavaIO/article.html
	 * 
	 */
	public static ArrayList<List<String>> readTextFile(String fileName) {

		FileReader file = null;

		ArrayList<List<String>> maze = new ArrayList<List<String>>();

		try {
			file = new FileReader(fileName);
			BufferedReader reader = new BufferedReader(file);
			String line = "";
			while ((line = reader.readLine()) != null) {

				List<String> l = Arrays.asList(line.trim().split(""));

				// Would use remove(), but it throws
				// UnSupportedOperationException
				maze.add(l.subList(1, l.size()));
			}

			reader.close();

		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (file != null) {
				try {
					file.close();
				} catch (IOException e) {
					// Ignore issues during closing
				}
			}
		}
		return maze;
	}
	
	/*
	 * Find the starting position of the given maze
	 */

	public static ArrayList<Integer> findStart(ArrayList<List<String>> maze)
			throws MazeDoesNotContainAStartingPoint {

		System.out.println(maze);

		int indexOf = -1;
		ArrayList<Integer> coordinates = new ArrayList<Integer>(2);

		boolean foundO = false;

		for (int i = 0; i < maze.size(); i++) {

			indexOf = maze.get(i).indexOf(STARTING_POINT_CHARACTER);
			if (-1 != indexOf) {
				coordinates.add(i);
				coordinates.add(indexOf);
				foundO = true;
			}
		}

		if (!foundO)
			throw new MazeDoesNotContainAStartingPoint();

		return coordinates;
	}
	
	public static void backtrack(ArrayList<List<Integer>> steps, int n) throws Exception {
		
		if (steps.size() > n)
			throw new Exception("IllegalBacktrack");
		
		steps = (ArrayList<List<Integer>>) steps.subList(0, steps.size() - n);
		
		
	}
}
